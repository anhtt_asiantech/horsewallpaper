package com.anhttvn.horselivewallpaper.util;

public class ConfigUtil {

    public static final String FOLDER_DOWNLOAD = "HORSE_WALLPAPER";
    public static final String FOLDER_ASSETS = "Wallpaper";
    /**
     * api
     */
    public static String KEY_NOTIFICATION = "Notification";

    public static String KEY_WALLPAPER = "Wallpaper";
    public static String INFORMATION ="Information";

//  Config Database

    public static  final String DATABASE = "HorseWallpaper.db";
    public static  final int VERSION = 3;
}
